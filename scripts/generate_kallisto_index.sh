#!/bin/bash

cd /home/efournie/projects/def-masirard/efournie/HumanGranulosa

mkdir -p output/kallisto
/home/efournie/kallisto_linux-v0.44.0/kallisto index -i output/kallisto/Homo_sapiens.GRCh38.cdna.all.fa.idx input/Homo_sapiens.GRCh38.cdna.all.fa