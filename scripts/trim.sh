# Generate trim jobs
for i in raw/*.bam
do
    sample=`echo $i | sed -e 's/.*HI.4800.004.NEBNext_Index_.*\.\(.*\)\.bam/\1/'`
    script=jobs/trim.$sample.sh
    mkdir -p jobs
    cat <<EOF > $script
#!/bin/bash
#module load picard
#java -Djava.io.tmpdir=/gs/scratch/$USER -XX:ParallelGCThreads=1 -Dsamjdk.buffer_size=4194304 -Xmx20G -jar $EBROOTPICARD/picard.jar SamToFastq \
# VALIDATION_STRINGENCY=LENIENT \
# INPUT=$i \
# FASTQ=$SCRATCH/$sample.R1.fastq.gz \
# SECOND_END_FASTQ=$SCRATCH/$sample.R2.fastq.gz


module load trimmomatic
mkdir -p output/trim/$sample
java -XX:ParallelGCThreads=1 -Xmx20G -jar $EBROOTTRIMMOMATIC/trimmomatic-0.36.jar PE \
  -threads 6 \
  -phred33 \
  $SCRATCH/$sample.R1.fastq.gz \
  $SCRATCH/$sample.R2.fastq.gz \
  output/trim/$sample.R1.fastq.gz \
  output/trim/$sample.single1.fastq.gz \
  output/trim/$sample.R2.fastq.gz \
  output/trim/$sample.single2.fastq.gz \
  ILLUMINACLIP:input/adapters.fa:2:30:15 \
  TRAILING:30 HEADCROP:4 \
  MINLEN:25
  
# rm $SCRATCH/$sample.R1.fastq.gz $SCRATCH/$sample.R2.fastq.gz  
EOF
    sbatch --time 2:00:00 --mem 24G --cpus-per-task 6 --account def-masirard -D `pwd` -e $script.stderr -o $script.stdout $script
done
