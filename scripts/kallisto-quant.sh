# Load module to get trimmomatic jar location.
OUTDIR=output/kallisto
mkdir -p output/jobs
mkdir -p $OUTDIR
for libr1 in output/trim/*R1.fastq.gz
do
  sample=`echo $libr1 | sed -e 's/output\/trim\/\(.*\).R1.fastq.gz/\1/'`
  libr2=output/trim/$sample.R2.fastq.gz
  
  mkdir -p $OUTDIR/$sample

  job_name=kallisto-quant.$sample
  job_script=output/jobs/$job_name.sh

  cat << EOF > $job_script  
#!/bin/bash  
module load kallisto
kallisto quant -t 6 -i output/Homo_sapiens.GRCh38.cdna.all.fa.idx -o output/kallisto/$sample $libr1 $libr2
EOF

  sbatch --mail-type=END,FAIL --mail-user=eric.fournier@fsaa.ulaval.ca -A def-masirard -D `pwd` -o $job_script.stdout -e $job_script.stderr -J $job_name --time=4:00:0 --mem=24G -N 1 -n 6 $job_script
done